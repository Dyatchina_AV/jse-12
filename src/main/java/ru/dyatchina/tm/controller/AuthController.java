package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.constant.TerminalConst;
import ru.dyatchina.tm.service.AuthService;

public class AuthController extends ABSController{
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public void printAbstractCommands() {
        System.out.println(TerminalConst.LOGIN + "<login> <password> do login");
        System.out.println(TerminalConst.LOGOUT + " do logout");
        System.out.println(TerminalConst.CHANGE_PASSWORD + " <old password> <new password> change current user password ");
        System.out.println(TerminalConst.CHANGE_USER_PASSWORD + " <login> <old password> <new password> change  user password ");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case TerminalConst.LOGIN:
                doLogin(commandWithArgs);
                return true;
            case TerminalConst.LOGOUT:
                authService.doLogout();
                return true;
            case TerminalConst.CHANGE_PASSWORD:
                changePassword(commandWithArgs);
                return true;
            case TerminalConst.CHANGE_USER_PASSWORD:
                changeUserPassword(commandWithArgs);
                return true;
            default:
                return false;
        }
    }

    private void changeUserPassword(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument!");
            return;
        }
        String login = commandWithArgs[1];
        String newPassword = commandWithArgs[2];
        boolean success = authService.changeUserPassword(login, newPassword);
        if (success) {
            System.out.println("Password changed");
        }
        else {
            System.out.println("Password not changed");
        }
    }

    private void changePassword(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument!");
            return;
        }
        String oldPassword = commandWithArgs[1];
        String newPassword = commandWithArgs[2];
        boolean success = authService.changeCurrentUserPassword(oldPassword, newPassword);
        if (success) {
            System.out.println("Password changed");
        }
        else {
            System.out.println("Password not changed");
        }
    }

    private void doLogin(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument!");
            return;
        }
        String login = commandWithArgs[1];
        String password = commandWithArgs[2];
        boolean success = authService.doLogin(login, password);
        if (success) {
            System.out.println("login success");
        }
        else {
            System.out.println("login failed");
        }
    }
}
