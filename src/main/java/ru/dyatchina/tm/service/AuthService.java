package ru.dyatchina.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.dyatchina.tm.entity.Role;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.repository.UserRepository;

public class AuthService {
        private final UserRepository userRepository;
        private User currentUser = null;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean doLogin(String login, String password) {
            User user = userRepository.findByLogin(login);
            if (user != null
            && user.getPassword().equals(DigestUtils.md5Hex(password))) {
                currentUser = user;
                return true;
            }
            return false;
        }

    public void doLogout() {
        currentUser = null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public boolean changeCurrentUserPassword(String oldPassword, String newPassword) {
        if (currentUser == null) {
            return false;
        }

        if (currentUser.getPassword().equals(DigestUtils.md5Hex(oldPassword))) {
            return false;
        }

        currentUser.setPassword(DigestUtils.md5Hex(newPassword));
        userRepository.save(currentUser);
        return true;
    }

    public boolean changeUserPassword(String login, String newPassword) {
        if (currentUser == null) {
            return false;
        }

        if (currentUser.getRole() == Role.ADMIN) {
            return false;
        }

        User user = userRepository.findByLogin(login);
        if (user == null) {
            return false;
        }

        currentUser.setPassword(DigestUtils.md5Hex(newPassword));
        userRepository.save(user);
        return true;
    }
}
