package ru.dyatchina.tm.service;

import ru.dyatchina.tm.entity.Role;
import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;
    private final AuthService authService;

    public TaskService(TaskRepository taskRepository, AuthService authService) {
        this.taskRepository = taskRepository;
        this.authService = authService;
    }

    public Task create(String name) {

        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public  Task create(String name, String description) {

        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name,description);
    }

    public  Task update(Long id, String name, String description) {

        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }


    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task removeByIndex(int index) {
        return taskRepository.removeByIndex(index);
    }

    public List<Task> findAll() {
        User currentUser = authService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        if (currentUser.getRole() == Role.USER) {
            return taskRepository.findAll();
        }
        else {
            return taskRepository.findByUserId(currentUser.getId());
        }
    }

    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    public Task findByProjectIdAndId(Long projectId, Long Id) {
        if (projectId == null || Id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, Id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAddByProjectId(projectId);
    }
}
